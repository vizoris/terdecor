
$(function() {

//Табы
$('.tabs-nav a').click(function(e) {
    e.preventDefault();
    var _id = $(this).attr('href');
    console.log( _id);
    var _targetElement = $('.tabs-wrap').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});



// 
var cartSlider = $('.cart-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  asNavFor: '.cart-slider__nav'
});


$('.cart-slider__nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.cart-slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  responsive: [
  
  {
    breakpoint: 768,
    settings: {
      slidesToShow: 3,
    }
  },
  ]
});










  //Показать/скрыть каталог
$('.btn-bg__catalog').click(function(event) {
  event.preventDefault();
  $(this).toggleClass('active');
  $('.catalog-menu').fadeToggle();
});

// Активкая кнопка в карточке товара
$('.product-item__btn .sprite').click(function() {
  $(this).toggleClass('active');
})


// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   });

}else {
  $('.dropdown').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}



$('.banner-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: false,
    dots: true
   
});


$('.interior-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: true,
    dots: false
   
});


$('.news-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    dots: false,
    responsive: [{
        breakpoint: 1199,
        settings: {
            slidesToShow: 4,
        }
        },
        {
        breakpoint: 992,
        settings: {
            slidesToShow: 3,
        }
        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
        }
        },
        {
        breakpoint: 550,
        settings: {
            slidesToShow: 1,
        }
        }
    ]
   
});


$('.reviews-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: true,
    dots: false
   
});



$('.video-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '0px',
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
        }
        }
    ]
   
});



$('.fancybox-product').fancybox({
  afterShow: function() {
      var productSlider = $('.modal-product__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        dots: false,
        asNavFor: '.modal-product__slider--nav'
      });


      $('.modal-product__slider--nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.modal-product__slider',
        dots: false,
        arrows: true,
        focusOnSelect: true,
        vertical: true,
        responsive: [
        {
          breakpoint: 990,
          settings: {
            vertical: false,
          }
        },
        {
          breakpoint: 479,
          settings: {
            slidesToShow: 3,
            vertical: false,
          }
        },
        ]
      });
  }

})







$('.fancybox-gallery').fancybox({
  afterShow: function() {
      var productSlider = $('.gallery-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: false,
        
        asNavFor: '.gallery-slider__nav'
      });


      $('.gallery-slider__nav').slick({
        slidesToShow: 12,
        slidesToScroll: 1,
        asNavFor: '.gallery-slider',
        dots: false,
        arrows: true,
        focusOnSelect: true,
        responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 9,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 7,
            vertical: false,
          }
        },
        {
          breakpoint: 550,
          settings: {
            slidesToShow: 5,
            vertical: false,
          }
        },
        {
          breakpoint: 370,
          settings: {
            slidesToShow: 3,
            vertical: false,
          }
        },
        ]
      });
  }

})





 // Стилизация селектов
$('select').styler();


$('.cart-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: true,
   
});


// FansyBox
 $('.fancybox').fancybox({});

// Скрыть\показать фильтры в сайдбаре
$('.filters-header').click(function() {
  $(this).toggleClass('active');
  $('.filters-item').fadeToggle();
})


// Меню в сайдбаре
$('.drop a').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('active');
  $(this).next('ul').fadeToggle();
})

// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})
$(window).scroll(function() {
  if ($(window).width() < 992) {
    if ( $(window).scrollTop() < 2000 ) {
      $('.sidebar-mobile__btn').fadeIn('200');
    } else {
      $('.sidebar-mobile__btn').fadeOut('200');
    }
  }
});



// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range




// Выбрать все в корзине
$("#selectAll").click(function(){
    console.log('ccc')
    $('.basket-item__check input:checkbox').not(this).prop('checked', this.checked);
});















})